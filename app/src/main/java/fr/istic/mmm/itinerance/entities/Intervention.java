package fr.istic.mmm.itinerance.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Intervention implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public String interventionType;
    public String address;
    public String heure;
    public String clientName;
    public String phoneNumber;
    public String comment;

    public Intervention() {
    }

    public Intervention(String heure, String interventionType, String address, String clientName, String phoneNumber, String comment) {
        this.heure = heure;
        this.interventionType = interventionType;
        this.address = address;
        this.clientName = clientName;
        this.phoneNumber = phoneNumber;
        this.comment = comment;
    }

    public Intervention(int id,String heure,String interventionType, String adress, String clientName, String phoneNumber, String comment) {
        this.id = id;
        this.heure = heure;
        this.interventionType = interventionType;
        this.address = adress;
        this.clientName = clientName;
        this.phoneNumber = phoneNumber;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getInterventionType() {
        return interventionType;
    }

    public String getAddress() {
        return address;
    }

    public String getClientName() {
        return clientName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInterventionType(String interventionType) {
        this.interventionType = interventionType;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
