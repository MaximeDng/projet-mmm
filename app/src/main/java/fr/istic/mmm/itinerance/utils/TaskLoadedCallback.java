package fr.istic.mmm.itinerance.utils;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
