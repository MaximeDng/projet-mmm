package fr.istic.mmm.itinerance.roomService;


import android.arch.persistence.room.Room;
import android.content.Context;

public class ServiceDB {

    /** Instance unique non préinitialisée */
    private static ServiceDB INSTANCE = null;


    private static ItineranceDataBase dataBase = null;

    /** Constructeur privé */
    private ServiceDB(Context context) {
        this.dataBase = Room.databaseBuilder(context,
            ItineranceDataBase.class, "database-name").build();
    }

    /** Point d'accès pour l'instance unique du singleton */
    public static synchronized ServiceDB getInstance(Context context)
    {
        if (INSTANCE == null)
        {   INSTANCE = new ServiceDB(context);
        }
        return INSTANCE;
    }

    public ItineranceDataBase getDB(){
        return this.dataBase;
    }
        
}
