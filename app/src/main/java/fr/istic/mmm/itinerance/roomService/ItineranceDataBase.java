package fr.istic.mmm.itinerance.roomService;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import fr.istic.mmm.itinerance.entities.Intervention;
import fr.istic.mmm.itinerance.entities.InterventionDAO;

@Database(entities = {Intervention.class}, version = 1)
public abstract class ItineranceDataBase extends RoomDatabase {
    public abstract InterventionDAO interventionDAO();
}

