package fr.istic.mmm.itinerance.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.istic.mmm.itinerance.R;
import fr.istic.mmm.itinerance.entities.Intervention;

public class ListInterventionAdapter extends RecyclerView.Adapter<ListInterventionAdapter.MyViewHolder>{

    private List<Intervention> interventions;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView id, heure, etat, address;

        public MyViewHolder(View view) {
            super(view);
            id = view.findViewById(R.id.item_id);
            heure = view.findViewById(R.id.item_heure_interv);
            etat = view.findViewById(R.id.item_etat_interv);
            address = view.findViewById(R.id.item_address);

        }
    }


    public ListInterventionAdapter(List<Intervention> interventions) {
        this.interventions = interventions;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_element, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Intervention intervention = interventions.get(position);
        holder.id.setText(intervention.getId()+"");
        holder.heure.setText(intervention.getHeure());
        holder.etat.setText(intervention.getInterventionType());
        holder.address.setText(intervention.getAddress());

    }

    public void addItems(List<Intervention> interventions){
        this.interventions = interventions;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return interventions.size();
    }
}
