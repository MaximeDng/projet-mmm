package fr.istic.mmm.itinerance.activity;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.istic.mmm.itinerance.R;
import fr.istic.mmm.itinerance.adapter.ListInterventionAdapter;
import fr.istic.mmm.itinerance.entities.Intervention;
import fr.istic.mmm.itinerance.listener.RecyclerTouchListener;
import fr.istic.mmm.itinerance.viewModel.InterventionViewModel;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ListInterventionAdapter mAdapter;
    private InterventionViewModel viewModel;
    private List<Intervention> interventionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.rv_interventions);

        mAdapter = new ListInterventionAdapter(new ArrayList<Intervention>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


         viewModel = ViewModelProviders.of(this).get((InterventionViewModel.class));

         viewModel.getAllInterventions().observe(MainActivity.this, new Observer<List<Intervention>>() {
             @Override
            public void onChanged(@Nullable List<Intervention> interventions) {
                 if (interventions.size() < 5){
                    populateDb();
                 }
                 else{
                     interventionList = interventions;
                     mAdapter.addItems(interventionList);
                 }

          }
      });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intervention intervention = interventionList.get(position);
                Intent intent = new Intent(MainActivity.super.getApplicationContext(), DetailActivity.class);
                intent.putExtra("intervention",intervention);
                startActivityForResult(intent,1);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == -1){
            Intervention intervention = (Intervention) data.getSerializableExtra("resultIntervention");
            viewModel.update(intervention);
        }
    }

    private void populateDb(){
        Intervention[] interventions = new Intervention[5];
        interventions[0] = new Intervention("14h","Maintenance","5 rue louis postel, 35000 Rennes","Client A","0676558970","");
        interventions[1] = new Intervention("16h","Installation compteur","13 Rue Claude Chappe, 35510 Cesson-Sévigné","Client B","0676558971","");
        interventions[2] = new Intervention("17h","Maintenance","Avenue du Phare du Grand Lejeon, 35520 Melesse","Client C","0676558972","");
        interventions[3] = new Intervention("17h30","Maintenance","9 rue louis postel, 35000 Rennes","Client D","0676558974","");
        interventions[4] = new Intervention("18h","Maintenance","276 Rue de Fougères, 35000 Rennes","Client E","0676558975","");
        viewModel.insertAll(interventions);
    }
}
