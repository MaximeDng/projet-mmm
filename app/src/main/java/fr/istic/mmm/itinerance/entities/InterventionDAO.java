package fr.istic.mmm.itinerance.entities;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface InterventionDAO {
    @Query("SELECT * FROM intervention")
    LiveData<List<Intervention>> getAllInterventions();

    @Update
    public void updateInterventions(Intervention... interventions);

    @Delete
    public void deleteInterventions(Intervention intervention);


    /*@Query("SELECT * FROM intervention")
    public List<Intervention> loadAllInterventions();*/

    @Query("SELECT * FROM intervention WHERE clientName == :cliName")
    public Intervention[] loadAllInterventionForThatClient(String cliName);

    @Insert
    void insertAll(Intervention... interventions);


}
