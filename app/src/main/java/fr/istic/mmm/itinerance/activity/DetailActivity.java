package fr.istic.mmm.itinerance.activity;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import fr.istic.mmm.itinerance.R;
import fr.istic.mmm.itinerance.entities.Intervention;
import fr.istic.mmm.itinerance.utils.GeoDestination;
import fr.istic.mmm.itinerance.utils.GetCoordinates;
import fr.istic.mmm.itinerance.viewModel.InterventionViewModel;

public class DetailActivity extends AppCompatActivity implements GeoDestination {
    TextView tv_id,tv_adresse, tv_type, tv_client, tv_contact;
    EditText e_commentaire;
    private InterventionViewModel viewModel;
    private String heureIntervention;
    private String latDestination;
    private String lngDestination;
    final static int TRAJET_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_intervention);
        Intervention intervention = (Intervention) this.getIntent().getExtras().getSerializable("intervention");
        instantiateViews();
        setTexts(intervention);
        new GetCoordinates(DetailActivity.this).execute(getUrlGeocode(intervention.getAddress()));
        heureIntervention = intervention.getHeure();
        viewModel = ViewModelProviders.of(this).get((InterventionViewModel.class));

    }



    public void onValidClick(View v){
        Intervention interventionUpdate = buildIntervention();
        Intent intent =  new Intent();
        intent.putExtra("resultIntervention",interventionUpdate);
        setResult(Activity.RESULT_OK,intent);
        finish();
    }

    public void onTrajetClick(View v){
        Intent intent = new Intent(this.getApplicationContext(), MapActivity.class);
        //intent.putExtra("interventionAddress",buildIntervention().getAddress());
        new GetCoordinates(DetailActivity.this).execute(getUrlGeocode(buildIntervention().getAddress()));
        intent.putExtra("latDestination", latDestination);
        intent.putExtra("lngDestination", lngDestination);

        //Log.d("GeoTest",latDestination);
        //Log.d("GeoTest",lngDestination);
        startActivityForResult(intent,TRAJET_REQUEST_CODE);
    }

    public void onAnnulerClick(View v){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private Intervention buildIntervention(){
        int id = Integer.valueOf(tv_id.getText().toString());
        String type = tv_type.getText().toString();
        String adress = tv_adresse.getText().toString();
        String clientName = tv_client.getText().toString();
        String phoneNumber = tv_contact.getText().toString();
        String comment = e_commentaire.getText().toString();

        return new Intervention(id,heureIntervention,type,adress,clientName,phoneNumber,comment);
    }

    private void instantiateViews(){
        tv_id = findViewById(R.id.detail_id);
        tv_adresse = findViewById(R.id.detail_adresse);
        tv_client = findViewById(R.id.detail_client);
        tv_type = findViewById(R.id.detail_type);
        tv_contact = findViewById(R.id.detail_contact);
        e_commentaire = findViewById(R.id.detail_commentaire);

    }

    private void setTexts(Intervention intervention){
        tv_id.setText(intervention.getId()+"");
        tv_adresse.setText(intervention.getAddress());
        tv_client.setText(intervention.getClientName());
        tv_type.setText(intervention.getInterventionType());
        tv_contact.setText(intervention.getPhoneNumber());
        e_commentaire.setText(intervention.getComment());
    }

    private String getUrlGeocode(String address){
        String parsedAddress = address.replaceAll(" ","+").replaceAll(",","+").replaceAll("-","+");

        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/geocode/"+output+"?"+"address="+parsedAddress+"&key="+getString(R.string.google_maps_key);
        Log.d("ParseAddress", url );
        return url;
    }

    @Override
    public void setDestination(String lat, String lng) {
        latDestination = lat;
        lngDestination = lng;
        Log.d("GeoTest",latDestination);
        Log.d("GeoTest",lngDestination);
    }
}
