package fr.istic.mmm.itinerance.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetCoordinates extends AsyncTask<String,Void,String> {

    Context originalContext;
    GeoDestination mContext;
    ProgressDialog waitSpinner;
    public GetCoordinates(Context mContext) {
        this.mContext = (GeoDestination) mContext;
        originalContext = mContext;
        waitSpinner = new ProgressDialog(originalContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response;
        try{
            String url = strings[0];
           GeoCodeDataHandler http = new GeoCodeDataHandler();
            response = http.getHTTPData(url);
            return response;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        waitSpinner = ProgressDialog.show(originalContext, "Please Wait ...", "Initializing the application ...", true);
    }

    @Override
    protected void onPostExecute(String s) {
        try{
            Log.d("GetCoordinate",s);
            JSONObject jsonObject = new JSONObject(s);

            String lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                    .getJSONObject("location").get("lat").toString();
            String lng = null;
                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").get("lng").toString();
            Log.d("GetCoordinate","lat = "+lat);
            Log.d("GetCoordinate","lng = "+lng);
            mContext.setDestination(lat,lng);
            waitSpinner.cancel();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
