package fr.istic.mmm.itinerance.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;

import fr.istic.mmm.itinerance.entities.Intervention;
import fr.istic.mmm.itinerance.roomService.ServiceDB;

public class InterventionViewModel extends AndroidViewModel {
    private final LiveData<List<Intervention>> interventions;
    private ServiceDB serviceDB;

    public InterventionViewModel(@NonNull Application application) {
        super(application);
        this.serviceDB = ServiceDB.getInstance(this.getApplication());
        interventions = this.serviceDB.getDB().interventionDAO().getAllInterventions();
    }

    public LiveData<List<Intervention>> getAllInterventions(){
        return this.interventions;
    }

    public void insertAll(Intervention... interventions){
       new insertAsyncTask(serviceDB).execute(interventions);
    }

    public void update(Intervention intervention){
        try {
            new updateAsyncTask(serviceDB).execute(intervention).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            Log.d("ERROR update async task", e.getCause()+"");
            e.printStackTrace();
        }
    }

    public void deleteIntervention(Intervention intervention){
        new deleteAsyncTask(serviceDB).execute(intervention);
    }

    private static class insertAsyncTask extends AsyncTask<Intervention, Void, Void> {

        private ServiceDB db;

        insertAsyncTask(ServiceDB appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Intervention... interventions) {

            db.getDB().interventionDAO().insertAll(interventions);
            return null;
        }


    }

    private static class updateAsyncTask extends AsyncTask<Intervention, Void, Void> {

        private ServiceDB db;

        updateAsyncTask(ServiceDB appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Intervention... interventions) {

            Intervention interventions1 = interventions[0];
            db.getDB().interventionDAO().updateInterventions(interventions[0]);
            return null;
        }


    }



    private static class deleteAsyncTask extends AsyncTask<Intervention, Void, Void> {

        private ServiceDB db;

        deleteAsyncTask(ServiceDB appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Intervention... interventions) {

            db.getDB().interventionDAO().deleteInterventions(interventions[0]);
            return null;
        }


    }
}
