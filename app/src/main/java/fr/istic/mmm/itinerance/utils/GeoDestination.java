package fr.istic.mmm.itinerance.utils;

public interface GeoDestination {
    void setDestination(String lat, String lng);

}
