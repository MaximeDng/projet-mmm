package fr.istic.mmm.itinerance;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import fr.istic.mmm.itinerance.entities.Intervention;
import fr.istic.mmm.itinerance.roomService.ServiceDB;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("fr.istic.mmm.itinerance", appContext.getPackageName());
    }

    @Test
    public void useDB() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        Intervention inter = new Intervention("coucou","chez moi","Mme Michu","022222222","je viens dire coucou.");
        ServiceDB.getInstance(appContext).getDB().interventionDAO().insertAll(inter);
        List<Intervention> interventions = Arrays.asList(ServiceDB.getInstance(appContext).getDB().interventionDAO().loadAllInterventions());
        for(Intervention interv:interventions){
            interv.getClientName();
        }

    }
}
